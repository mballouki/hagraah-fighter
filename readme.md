Utilisation Hagraah Fighter:

Avant toute utilisation, v�rifier que le module pygame est bien install�. Sinon "pip install pygame"

Au lancement du jeu : 
1)	Cliquez n�importe o� sur la page d�accueil 

2)	La page suivante vous indique que le bleu doit choisir son 1er personnage (la couleur du fond de la page indique � quel joueur il est le tour de choisir son personnage)

3)	Le joueur bleu doit donc double cliquer sur l�image du personnage de son choix (il doit en choisir 3 sachant que si un personnage est d�j� choisi, il ne peut pas le prendre)

4)	C�est maintenant au tour du rouge de choisir ses personnages

5)	Apr�s le choix des 3 personnages pour les 2 joueurs, il faut cliquer une nouvelle fois n�importe o� sur l��cran pour lancer la partie
6)	Les joueurs sont donc plac�s dans une carte al�atoire et leurs touches sont :
Pour le rouge : � zqsd � pour se d�placer et � e � pour tirer
Pour le bleu : les touches directionnelles pour se d�placer ainsi que la barre d�espace pour tirer

7)	Chaque joueur dispose de 3 vies, sa barre de vie est repr�sent�e par la barre verte au dessus du joueur, plus il prend de d�g�ts, plus sa barre deviendra rouge jusqu�� la perte d�une vie

8)	Apr�s la perte de ses 3 vies, le perdant a une chance de revenir en jeu si il r�pond correctement � une question sur la cybers�curit�

9)	Le joueur doit r�pondre au QCM en cliquant sur le rectangle rouge qui entoure la r�ponse qu�il pense correcte. Si sa r�ponse est correcte, il peut continuer � jouer mais n�a qu�une seule vie. Si elle est mauvaise, le jeu se termine en c�l�brant la victoire de son adversaire

10)	Le processus se r�p�te si la r�ponse est correcte jusqu�� ce que le perdant r�ponde faussement au QCM.

11)	Pour quitter le jeu ou recommencer, il suffit de cliquer sur la croix en haut � droite


